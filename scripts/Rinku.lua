local rinku = {}

local entities = require("SynchronyExtendedAPI.extended.Entities")
local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"

local debugConsole = require("SynchronyExtendedAPI.utils.DebugConsole")
local apiUtils = require("SynchronyExtendedAPI.utils.APIUtils")

local ecs = require "system.game.Entities"

local object = require("necro.game.object.Object")
local player = require("necro.game.character.Player")
local animations = require("necro.render.level.Animations")
local animationTimer = require("necro.render.AnimationTimer")
local action = require("necro.game.system.Action")
local event = require("necro.event.Event")
local commonWeapon = require("necro.game.data.item.weapon.CommonWeapon")
local soundGroups = require("necro.audio.SoundGroups")
local voice = require "necro.audio.Voice"
local sound = require "necro.audio.Sound"
local dig = require "necro.game.tile.Dig"
local move = require "necro.game.system.Move"

local inventoryHUD = require "necro.render.hud.inventory.InventoryHUD"
local hudLayout = require "necro.render.hud.HUDLayout"

local components = require("necro.game.data.Components")
local field = components.field
local constant = components.constant

local frameTimesDirectionalMove = {0, 0.5}
local frameTimesDirectionalJump = {0, 0.07, 0.14, 0.2}
local frameTimesDirectionalAttack = {0, 0.1, 0.5, 0.6}
local frameTimesDirectionalDig = {0, 0.2, 0.6}

function rinku.register()
    components.register {
        directionalAttackAnimation = {
            constant.table("frames"),
            constant.table("variants"),
            field.bool("active", false),
        },
        directionalDigAnimation = {
            constant.table("frames"),
            constant.table("variants"),
            field.bool("active", false),
        },
        directionalJumpAnimation = {
            constant.table("frames"),
            constant.table("variants"),
            field.bool("active", false),
        },
        itemFeetAction = {},
    }

    entities.addPlayer(namedPlayers.Cadence, {
        name = "Rinku",
        data = {},
        components = {
            {
                -- body
                wired = false,
                playableCharacter = {
                    lobbyOrder = 201,
                },
                sprite = {
                    texture = "mods/LegendOfRinku/gfx/Rinku/Rinku.png",
                    width = 16,
                    height = 16,
                },
                voiceHitChain = false,
                voiceDig = {
                    sound = "LegendOfRinku_dig",
                },
                soundHit = false,
                voiceHit = {
                    sound = "LegendOfRinku_hurt"
                },
                voiceDeath = { sound = "LegendOfRinku_dying" },
                voiceDescend = { sound = "LegendOfRinku_fall" },
                characterFacingMirrorX = false,
                directionalAnimation = {
                    frames = {
                        frames = {1, 2},
                        times = frameTimesDirectionalMove,
                    },
                    variants = {
                        -- direction
                        [action.Direction.DOWN] = {
                            frames = {1, 2},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.UP] = {
                            frames = {3, 4},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.RIGHT] = {
                            frames = {5, 6},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.UP_RIGHT] = {
                            frames = {5, 6},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.DOWN_RIGHT] = {
                            frames = {5, 6},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.LEFT] = {
                            frames = {7, 8},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.UP_LEFT] = {
                            frames = {7, 8},
                            times = frameTimesDirectionalMove,
                        },
                        [action.Direction.DOWN_LEFT] = {
                            frames = {7, 8},
                            times = frameTimesDirectionalMove,
                        },
                    },
                },
                LegendOfRinku_directionalAttackAnimation = {
                    frames = {
                        frames = {1, 2},
                        times = frameTimesDirectionalMove,
                    },
                    variants = {
                        -- direction
                        [action.Direction.DOWN] = {
                            frames = {15, 16, 1, 2},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.UP] = {
                            frames = {13, 14, 3, 4},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.LEFT] = {
                            frames = {9, 10, 7, 8},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_LEFT] = {
                            frames = {9, 10, 7, 8},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_LEFT] = {
                            frames = {9, 10, 7, 8},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.RIGHT] = {
                            frames = {11, 12, 5, 6},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_RIGHT] = {
                            frames = {11, 12, 5, 6},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_RIGHT] = {
                            frames = {11, 12, 5, 6},
                            times = frameTimesDirectionalAttack,
                            offBeat = {
                                duration = 0.5,
                                hold = true,
                            },
                        },
                    },
                },
                LegendOfRinku_directionalDigAnimation = {
                    frames = {
                        frames = {1, 2},
                        times = frameTimesDirectionalMove,
                    },
                    variants = {
                        -- direction
                        [action.Direction.DOWN] = {
                            frames = {17, 18, 1},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.UP] = {
                            frames = {19, 20, 3},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.LEFT] = {
                            frames = {21, 22, 8},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_LEFT] = {
                            frames = {21, 22, 8},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_LEFT] = {
                            frames = {21, 22, 8},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.RIGHT] = {
                            frames = {23, 24, 5},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_RIGHT] = {
                            frames = {23, 24, 5},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_RIGHT] = {
                            frames = {23, 24, 5},
                            times = frameTimesDirectionalDig,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                    },
                },
                LegendOfRinku_directionalJumpAnimation = {
                    frames = {
                        frames = {1, 2},
                        times = frameTimesDirectionalMove,
                    },
                    variants = {
                        -- direction
                        [action.Direction.DOWN] = {
                            frames = {25, 26, 27, 1},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 0.8,
                                hold = true,
                            },
                        },
                        [action.Direction.UP] = {
                            frames = {28, 29, 30, 4},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.RIGHT] = {
                            frames = {34, 35, 36, 6},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_RIGHT] = {
                            frames = {34, 35, 36, 6},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_RIGHT] = {
                            frames = {34, 35, 36, 6},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.LEFT] = {
                            frames = {31, 32, 33, 8},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.UP_LEFT] = {
                            frames = {31, 32, 33, 8},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                        [action.Direction.DOWN_LEFT] = {
                            frames = {31, 32, 33, 8},
                            times = frameTimesDirectionalJump,
                            offBeat = {
                                duration = 1,
                                hold = true,
                            },
                        },
                    },
                },
                positionalSprite = {
                    offsetX = 4,
                    offsetY = 10,
                },
                characterEquipmentSpriteRow = false,
                bestiary = {
                    image = "ext/bestiary/bestiary_clone.png",
                    focusX = 236,
                    focusY = 156,
                },
                initialEquipment = {
                    items = {
                        "LegendOfRinku_Bomb",
                        "LegendOfRinku_ShovelBasic",
                        "LegendOfRinku_WeaponSword",
                        "LegendOfRinku_FeetFeather",
                    },
                },
            },
            {
                -- head
                sprite = false,
                characterEquipmentSpriteRow = false,
            },
        },
    })

    local function isRinku(entity)
        return entity and ecs.getEntityTypeName(entity) == "LegendOfRinku_Rinku"
    end

    entities.addItem(
        namedItems.WeaponDagger,
        {
            name = "WeaponSword",
            data = {},
            components = {
                soundWeaponHit = {
                    sound = "LegendOfRinku_swordSlash",
                },
                sprite = {
                    texture = "mods/LegendOfRinku/gfx/Rinku/weapon_rinkusword.png",
                    width = 18,
                    height = 18,
                },
                weaponPattern = {
                    pattern = commonWeapon.pattern({
                        swipe = "rinkusword",
                        tiles = {
                            {
                                offset = {1, 0},
                            },
                        },
                    })
                }
            },
        }
    )

    entities.addItem(namedItems.ShovelBasic, {
        name = "ShovelBasic",
        data = {},
        components = {
            sprite = {
                texture = "mods/LegendOfRinku/gfx/Rinku/shovel_rinku.png",
                width = 18,
                height = 18,
            },
        },
    })

    entities.addItem(namedItems.Bomb, {
        name = "Bomb",
        data = {},
        components = {
            sprite = {
                texture = "mods/LegendOfRinku/gfx/Rinku/bomb_rinku.png",
                width = 18,
                height = 18,
            },
        },
    })

    apiUtils.safeOverrideEvent(
        event.inventoryAddItem,
        "convertOnPickup",
        {
            order = "convert",
            filter = "itemConvertOnPickup",
            sequence = 1,
        },
        function (func, ev)
            if isRinku(ev.holder)
                    and ev.item.itemConvertOnPickup.targetType == namedItems.Bomb.name then
                object.convert(ev.item, "LegendOfRinku_Bomb")
            else
                func(ev)
            end
        end)

    entities.addItem(namedItems.FeetBootsLeaping, {
        name = "FeetFeather",
        data = {},
        components = {
            sprite = {
                texture = "mods/LegendOfRinku/gfx/Rinku/feet_feather.png",
                width = 18,
                height = 18,
            },
            itemMoveAmplifier = false,
            itemToggleable = false,
            LegendOfRinku_itemFeetAction = {},
        },
    })

    apiUtils.safeAddEvent(
        event.holderSpecialAction,
        "feetActionItem",
        {
            order = "toggle",
            filter = "LegendOfRinku_itemFeetAction",
            sequence = -1
        },
        function (ev)
            if ev.result == nil and ev.action == action.Special.THROW then
                --character.hopInPlace(ev.holder)
                local dx, dy = action.getMovementOffset(ev.holder.facingDirection.direction)
                move.relative(ev.holder, dx * 2, dy * 2, move.Type.FLYING)
                ev.result = action.Result.MOVE
                if isRinku(ev.holder) then
                    sound.play("LegendOfRinku_jump", ev.holder.position.x, ev.holder.position.y)
                    animationTimer.playAnimation(ev.holder.id)
                    ev.holder.LegendOfRinku_directionalJumpAnimation.active = true
                end
            end
        end)

    inventoryHUD.registerSlot {
        name = "active",

        slot = "feet",
        element = hudLayout.Element.ACTIONS,
        index = 1,
        image = "mods/LegendOfRinku/gfx/Rinku/hud_slot_boots_jump.png",
        showPickupAnimation = false,
        showAmmoCounter = false,
        keyBindings = {
            action.Special.THROW
        },
        slotVisibility = function (slot)
            return slot.item:hasComponent("LegendOfRinku_itemFeetAction")
        end,
    }
    
    -- animations --

    apiUtils.safeAddEvent(
        event.swipe,
        "rinkusword",
        "rinkusword",
        function (ev)
            local direction = ev.direction

            local offsetX, offsetY = -4, -2
            local firstFrame = 1
            if direction == action.Direction.RIGHT then
                firstFrame = 10
                offsetX = offsetX - 16
            elseif direction == action.Direction.LEFT then
                firstFrame = 7
                offsetX = offsetX - 32
            elseif direction == action.Direction.UP then
                firstFrame = 1
                offsetX = offsetX - 16
            elseif direction == action.Direction.DOWN then
                firstFrame = 4
                offsetX = offsetX - 32
                offsetY = offsetY + 16
            end

            ev.entity.swipe.texture = "mods/LegendOfRinku/gfx/Rinku/swipe_rinkusword.png"
            ev.entity.swipe.frameCount = 3
            ev.entity.swipe.firstFrame = firstFrame
            ev.entity.swipe.width = 32
            ev.entity.swipe.height = 32
            ev.entity.swipe.offsetX = offsetX
            ev.entity.swipe.offsetY = offsetY
            ev.entity.swipe.angle = 0
            ev.entity.swipe.duration = 120
            ev.entity.swipe.mirrorX = false
            ev.entity.swipe.mirrorY = false
        end)
    

    apiUtils.safeAddEvent(
        event.animateObjects,
        "applyDirectionalAnimations",
        {
            order = "directional",
            sequence = 1,
        },
        function ()
            animations.apply(
                "LegendOfRinku_directionalAttackAnimation",
                function (entity)
                    return entity.LegendOfRinku_directionalAttackAnimation.active, entity.facingDirection.direction
                end)
            animations.apply(
                "LegendOfRinku_directionalDigAnimation",
                function (entity)
                    return entity.LegendOfRinku_directionalDigAnimation.active, entity.facingDirection.direction
                end)
            animations.apply(
                "LegendOfRinku_directionalJumpAnimation",
                function (entity)
                    return entity.LegendOfRinku_directionalJumpAnimation.active, entity.facingDirection.direction
                end)
        end)

    apiUtils.safeAddEvent(
        event.turn,
        "resetAnimation",
        {
            order = "resetAnimations",
            sequence = 1,
        },
        function(ev)
            for _, entity in ecs.liveTypesWithComponents {"LegendOfRinku_directionalAttackAnimation"} do
                while entity:next() do
                    if entity.gameObject.active then
                        entity.LegendOfRinku_directionalAttackAnimation.active = false
                    end
                end
            end
            for _, entity in ecs.liveTypesWithComponents {"LegendOfRinku_directionalDigAnimation"} do
                while entity:next() do
                    if entity.gameObject.active then
                        entity.LegendOfRinku_directionalDigAnimation.active = false
                    end
                end
            end
            for _, entity in ecs.liveTypesWithComponents {"LegendOfRinku_directionalJumpAnimation"} do
                while entity:next() do
                    if entity.gameObject.active then
                        entity.LegendOfRinku_directionalJumpAnimation.active = false
                    end
                end
            end
        end)

    apiUtils.safeAddEvent(
        event.objectCheckAttack,
        "playAttackAnimation",
        {
            order = "weapon",
            filter = "LegendOfRinku_directionalAttackAnimation",
            sequence = 1,
        },
        function (ev)
            if not ev.suppressed and ev.result and ev.result.success then
                ev.entity.LegendOfRinku_directionalAttackAnimation.active = true
                animationTimer.playAnimation(ev.entity.id)
            end
        end)
    
    apiUtils.safeAddEvent(
        event.objectDig,
        "playDigAnimation",
        {
            order = "sprite",
            filter = "LegendOfRinku_directionalDigAnimation",
            sequence = 1,
        },
        function (ev)
            if ev.success and ev.tileInfo.digResistance >= 0 then
                ev.entity.LegendOfRinku_directionalDigAnimation.active = true
                animationTimer.playAnimation(ev.entity.id)
            end
        end)

    -- sound overrides --
    apiUtils.safeOverrideEvent(
        event.objectTakeDamage,
        "hitSound",
        {
            order = "sound",
            filter = {"position", "voiceHit"},
            sequence = 1
        },
        function (func, ev)
            if isRinku(ev.attacker) then
                if not ev.suppressed and ev.damage > 0 and not ev.squishy and ev.survived then
                    voice.play(ev.entity, "LegendOfRinku_enemyHit")
                end
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectTakeDamage,
        "squishSound",
        {
            order = "sound",
            filter = { "position", "voiceSquish" },
            sequence = 1
        },
        function (func, ev)
            if isRinku(ev.attacker) then
                -- silent suppress
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectTakeDamage,
        "genericHitSound",
        {
            order = "sound",
            filter = { "position", "soundHit" },
            sequence = 1
        },
        function (func, ev)
            if isRinku(ev.attacker) then
                -- silent suppress
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectTakeDamage,
        "suppressedHitSound",
        {
            order = "sound",
            filter = { "position", "voiceHitSuppressed" },
            sequence = 1
        },
        function (func, ev)
            if isRinku(ev.attacker) then
                if ev.suppressed then
                    voice.play(ev.entity, "LegendOfRinku_deflectSound")
                end
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectDeath,
        "deathSound",
        {
            order = "sound",
            filter = { "position", "voiceDeath" },
            sequence = 1,
        },
        function (func, ev)
            if isRinku(ev.killer) then
                voice.play(ev.entity, "LegendOfRinku_enemyDie")
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectDeath,
        "genericDeathSound",
        {
             order = "sound",
             filter = { "position", "soundDeath" },
             sequence = 1,
        },
        function (func, ev)
            if isRinku(ev.killer) then
                if ev.entity.soundDeath.sound == "chestOpen" then
                    sound.play("LegendOfRinku_chest", ev.entity.position.x, ev.entity.position.y)
                end
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.inventoryCollectItem,
        "playItemPickupSound",
        {
            order = "sound",
            filter = "itemPickupSound",
            sequence = 1,
        },
        function (func, ev)
            if isRinku(ev.holder) then
                if not ev.silent and player.isFocusedEntity(ev.holder) then
                    if ev.item.itemPickupSound.sound == "pickupArmor"
                            or ev.item.itemPickupSound.sound == "pickupWeapon" then
                        sound.play("LegendOfRinku_item")
                    elseif ev.item.itemPickupSound.sound == "pickupGold" then
                        sound.play("LegendOfRinku_itemRupee")
                    else
                        sound.play("LegendOfRinku_itemGeneric")
                    end
                end
            else
                func(ev)
            end
        end)

    apiUtils.safeOverrideEvent(
        event.objectDig,
        "digSound",
        {
            order = "sound",
            filter = {"position", "voiceDig"},
            sequence = 1,
        },
        function (func, ev)
            if isRinku(ev.entity) then
                if ev.success then
                    if ev.tileInfo.digResistance >= 0 then
                        voice.play(ev.entity, ev.entity.voiceDig)
                    elseif ev.tileInfo.digResistance == dig.Strength.DOOR then
                        voice.play(ev.entity, "LegendOfRinku_door")
                    end
                end
            else
                func(ev)
            end
        end)
    
    apiUtils.safeAddEvent(
        event.contentLoad,
        "addCustomSounds",
        {
            order = "sounds",
            sequence = 1
        },
        function (ev)
            soundGroups.addGroup(
                "LegendOfRinku_swordSlash",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Sword_Slash1.wav",
                    "mods/LegendOfRinku/sfx/Rinku/LA_Sword_Slash2.wav",
                    "mods/LegendOfRinku/sfx/Rinku/LA_Sword_Slash3.wav",
                    "mods/LegendOfRinku/sfx/Rinku/LA_Sword_Slash4.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_stairs",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Stairs.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_dying",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Link_Dying.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_hurt",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Link_Hurt.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_fall",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Link_Fall.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_dig",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Shovel_Dig.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_enemyHit",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Enemy_Hit.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_enemyDie",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Enemy_Die.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_deflectSound",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Shield_Deflect.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_door",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Dungeon_DoorSlam.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_chest",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Chest_Open.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_item",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Fanfare_Item.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_itemRupee",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Get_Rupee.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_itemGeneric",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Get_Item.wav",
                })
            soundGroups.addGroup(
                "LegendOfRinku_jump",
                {
                    "mods/LegendOfRinku/sfx/Rinku/LA_Link_Jump.wav",
                })
        end)
end

return rinku